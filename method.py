#!/usr/bin/env python

from contextlib import AbstractContextManager
from functools import wraps
from pathlib import Path
import shutil
from zipfile import ZipFile

from cortextlib import CortextMethod

import storage

class Nbib(CortextMethod):
    """
    Parser method for Nbib fromatted Pubmed exports employing Parscival.
    """

    def __init__(self, job):
        """
        Initialize base class and treat parameters.
        """
        super().__init__(job)
        # Add parameter processing here
        self.params.clear()
        self.data_paths = []

    def __exit__(self, exc_type, exc_value, traceback):
        """
        Cleanup logic if any. AbstractContextManager requires declaring this method.
        """
        for f in self.data_paths:
            f.close()
        shutil.rmtree(self.tmp_zip_extract_dir)

    def main(self):
        """
        Called after __init__().
        """
        import parscival.worker

        parscival.worker.log = self.job.log

        spec_path = (
            Path(parscival.__file__).parents[1] / "parscival_specs" / "pubmed-nbib.yaml"
        )
        zip_data_path = self.job.db  # CortextManager passes the dataset as the job's db
        self.tmp_zip_extract_dir = self.job.dir / "extracted_zip_data.tmp"
        self.tmp_zip_extract_dir.mkdir()
        ZipFile(zip_data_path).extractall(self.tmp_zip_extract_dir)
        for x in self.tmp_zip_extract_dir.iterdir():
            if x.suffix == ".nbib":
                self.data_paths.append(x.open("rb"))
        log = self.job.log
        sql_path = self.job.dir / (self.job.db.name + ".cortext.sqlite")
        db_path = self.job.dir / (self.job.db.name + ".db")
        db_con = storage.create_database(db_path, log)
        with spec_path.open() as spec_file:
            with HookProgressParscival(self.job.progress, parscival):
                if not parscival.worker.process_datasets(
                    spec_file, sql_path, self.data_paths
                ):
                    raise RuntimeError("Parsing finished with errors")
                log.info("Registering database")
                storage.register(db_path, db_con, self.job.db.name, self.job.db_meta_update, log)
            



class HookProgressParscival(AbstractContextManager):
    def __init__(self, progress, parscival):
        """
        Doesn't yet account for database creation, but we might do fine without that.
        """
        self.load_datasets_info = parscival.worker.load_datasets_info

        @wraps(self.load_datasets_info)
        def load_datasets_info_wrapper(parsing_spec, parsing_data, *args, **kwargs):
            ret = self.load_datasets_info(parsing_spec, parsing_data, *args, **kwargs)
            progress.set_total(parsing_data["stats"]["total"])
            return ret

        parscival.worker.load_datasets_info = load_datasets_info_wrapper

        self.parse_document = parscival.worker.parse_document

        @wraps(self.parse_document)
        def parse_document_wrapper(*args, **kwargs):
            ret = self.parse_document(*args, **kwargs)
            progress.advance()
            return ret

        parscival.worker.parse_document = parse_document_wrapper

        self.parscival = parscival

    def __exit__(self, exc_type, exc_value, traceback):
        self.parscival.worker.load_datasets_info = self.load_datasets_info
        self.parscival.worker.parse_document = self.parse_document

    
