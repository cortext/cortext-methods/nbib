#!/usr/bin/env python

from functools import wraps
import sqlite3
import yaml


def create_database(db_path, log):
    """
    Creates an SQLite database and returns a connection.
    """
    if db_path != ":memory:":
        assert not db_path.exists()
    log.info(f"Connecting to new database at {db_path}")
    return sqlite3.connect(db_path)


def commit(func):
    @wraps(func)
    def _commits(self, *args, **kwargs):
        with self.con:
            return func(self, *args, **kwargs)

    return _commits


class Database(object):
    """
    Stores data in a database through the given connection.
    """

    def __init__(self, con):
        self.con = con
        self.known_fields = set()

    @commit
    def store_document(self, source, id, data):
        """
        Stores the data for a single document in the database.
        """
        for field_name, field_value in data.items():
            if field_name not in self.known_fields:
                self.create_table(field_name, self.field_type(field_name))
                self.known_fields.add(field_name)
            for rank, rank_value in enumerate(field_value):
                for parserank, parserank_value in enumerate(rank_value):
                    self.insert_record(
                        field_name, str(source), id, rank, parserank, parserank_value
                    )

    def create_table(self, table_name, data_type):
        """
        Implementation note:
        Using f-string as DB-API doesn't do parameter substitution on create table.
        """
        sql = f"""
        CREATE TABLE {table_name} (
          file text,
          id integer,
          rank integer,
          parserank integer,
          data {data_type}
        );
        """
        self.con.execute(sql)

    def insert_record(self, table_name, source, id, rank, parserank, data):
        sql = f"INSERT INTO {table_name} VALUES (:file, :id, :rank, :parserank, :data);"
        self.con.execute(
            sql, dict(file=source, id=id, rank=rank, parserank=parserank, data=data)
        )

    @staticmethod
    def field_type(name):
        """
        Keeps a list of nontext fields. Default is text.
        So far, the list is empty.
        """
        nontext_fields = {'ISIpubdate': "integer"}
        return nontext_fields.get(name, "text")


def write_to_fs(files, result_path):
    """Outputs contents to files, appending in case file already exists."""
    for name, contents in files.items():
        with (result_path / name).open("a") as f:
            f.write(contents)


def register(db_path, con, db_name, register_func, log):
    all_fields = con.execute(
        """
        SELECT name FROM sqlite_master
        WHERE type='table'
        ORDER BY name;
        """
    ).fetchall()
    all_fields = [x[0] for x in all_fields]
    textual_fields = con.execute(
        """
        SELECT name FROM sqlite_master
        WHERE type='table'
        AND (SELECT type FROM PRAGMA_TABLE_INFO(sqlite_master.name)
             WHERE name='data')='text'
        ORDER BY name;
        """
    ).fetchall()
    textual_fields = [x[0] for x in textual_fields]
    meta = {
        "alltables": all_fields,
        "corpus_type": "istex",
        "extension": "db",
        "file": str(db_path),
        "indexed": False,
        "origin": "dataset",
        "structure": "reseaulu",
        "tablenames": all_fields.copy(),  # can CM handle yaml pointers?
        "textual_fields": textual_fields,
        "totaltables": all_fields.copy(),  # can CM handle yaml pointers?
        "uri": db_name,
        "version": 7,
    }
    meta_path = db_path.parent / (db_name + ".db.yaml")
    assert not meta_path.exists()
    with meta_path.open("w") as f:
        yaml.dump(meta, f, width=float("inf"))  # CM issue with finite width

