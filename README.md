# nbib parser

Parse nbib formatted data into a CortextManager database using Parscival. Nbib is a RIS-like format employed by Pubmed for simplified data exports.
